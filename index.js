const validate = require('./validate');
const methods = require('./methods');

module.exports = { validate, methods }
