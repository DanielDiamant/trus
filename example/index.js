const express = require('express');
const menu = require('./menu');
var trus = require("../index.js");

var app = express();
app.get("/", (req,res) => res.send(menu) )
app.get("/simpleValidation-success",
  (req,res,next) => {
    trus.validate( trus.methods.any() )()
      .then(res.send("ok"))
      .catch(next)
  }
);
app.get("/simpleValidation-fail",
  (req,res,next) => {
    trus.validate( trus.methods.none() )()
      .then(res => res.send("ok"))
      .catch(next)
  }
);
app.get("/expressValidation-success",
  trus.validate.express( trus.methods.any() ),
  (req,res) => res.send("ok")
)
app.get("/expressValidation-fail",
  trus.validate.express( trus.methods.none() ),
  (req,res) => res.send("ok")
)

var customMethod = req =>
  req.params.x == "yes" ? Promise.resolve() : Promise.reject();

app.get("/customMethod/:x",
  trus.validate.express( customMethod ),
  (req,res) => res.send("ok")
)

var extractRole = req => req.params.role; // can be queries from a db for instance
var matchRole = role => req => new Promise(function(resolve, reject) {
  var userRole = extractRole(req);
  if(userRole == role)
    return resolve();
  reject("not " + role);
});
var roles = {
  manager: matchRole("manager"),
  admin: matchRole("admin"),
  viewer: matchRole("viewer")
}
app.get("/role-based-validation/:role",
  trus.validate.express( roles.manager ),
  (req,res) => res.send("ok")
)
app.get("/role-based-validation-many/:role",
  trus.validate.express( trus.methods.someOf( roles.manager, roles.admin ) ),
  (req,res) => res.send("ok")
)


app.listen(80);
