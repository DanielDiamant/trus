var Link = function(url, name){
  this.url = url;
  this.name = name || url;
}
Link.prototype.render = function(){
  return `<a href="${this.url}">${this.name}</a>`
};

var options = [
  new Link("./simpleValidation-success", "Simple Validation - success"),
  new Link("./simpleValidation-fail", "Simple Validation - fail"),
  new Link("./expressValidation-success", "Express Validation - success"),
  new Link("./expressValidation-fail", "Express Validation - fail"),
  new Link("./customMethod/yes", "Custom method - success"),
  new Link("./customMethod/no", "Custom method - fail"),
  new Link("./role-based-validation/manager", "role-based - success"),
  new Link("./role-based-validation/viewer", "role-based - fail"),
  new Link("./role-based-validation-many/manager", "role-based(many) - success"),
  new Link("./role-based-validation-many/viewer", "role-based(many) - fail")
]

module.exports = options
  .map(i => i.render())
  .join("<br />")
