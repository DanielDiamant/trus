module.exports = method => context => new Promise(function(resolve, reject) {
  method(context)
    .then(reject)
    .catch(resolve)
});
