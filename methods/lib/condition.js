module.exports = condition => context => new Promise(function(resolve, reject) {
  if( condition(context) )
    return resolve();
  reject( new Error("condition mismatch for input:") )
});
