module.exports = function(){
  var validations = Array.from(arguments)
  return context => Promise.all(
    validations.map( v =>
      Promise.resolve( v(context) )
        .then( () => null )
        .catch( e => e || new Error("unauthorized") )
    )
  ).then(res => {
    var errors = res.filter( i => i );
    if( errors.length != 0 )
      throw errors;
    return;
  })
}
