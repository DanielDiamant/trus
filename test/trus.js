var trus = require("../index.js");

describe("trus", () => {
  it("has a validate method", () => trus.validate.should.be.a.Function() );
  it("has a methods collection", () => trus.methods.should.be.an.Object() );
});
