var trus = require("../index.js");

describe("validate", () => {
  it("resolves on positive validation", () => trus.validate( () => Promise.resolve() )().should.be.fulfilled() )
  it("rejects on negative validation", () => trus.validate( () => Promise.reject() )().should.be.rejected() )
  it("injects context to validate method", () => {
    var id = "123";
    return new Promise(function(resolve, reject) {
      trus.validate( (context) => {
        if(context == id)
          return resolve();
        reject(new Error("expected context to be: " + id + ", but got: " + context) );
      } )(id);
    });
  })
})
