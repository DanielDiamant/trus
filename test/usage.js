var trus = require("../index.js");
var validate = require("../index.js").validate;
var { any, none, allOf, someOf, condition, not } = require("../index.js").methods;

require('should');

describe("usage", () => {
  it("has validate", () => validate.should.be.a.Function());
  it("has any", () => any.should.be.a.Function());
  it("has none", () => none.should.be.a.Function());
  it("has allOf", () => allOf.should.be.a.Function());
  it("has someOf", () => someOf.should.be.a.Function());
  it("has condition", () => condition.should.be.a.Function());
  it("has not", () => not.should.be.a.Function());
})
