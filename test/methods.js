require('should');
var trus = require("../index.js");
var validate = trus.validate;
var { any, none, allOf, someOf, condition, not } = trus.methods;

describe("methods", () => {
  it("any", () => validate( any )().should.be.resolved() );
  it("none", () => validate( none )().should.be.rejected() );
  it("allOf - positive", () => validate( allOf( any, any ) )().should.be.resolved() );
  it("allOf - negative", () => validate( allOf( any, none ) )().should.be.rejected() );
  it("someOf - positive", () => validate( someOf( any, none ) )().should.be.resolved() );
  it("someOf - negative", () => validate( someOf( none, none ) )().should.be.rejected() );
  it("condition - positive", () => validate( condition( context => context == 1) )(1).should.be.resolved() );
  it("condition - positive", () => validate( condition( context => context == 1) )(2).should.be.rejected() );
  it("not - positive", () => validate( not( none ) )().should.be.resolved() );
  it("not - negative", () => validate( not( any ) )().should.be.rejected() );
});
