var validate = method => context => method(context);
module.exports = validate;
